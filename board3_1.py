
import math, cmath
from numpy import *

lam = 2*pi

eps_0 = 1.0

Z0 = 1/sqrt(eps_0)

# two media

eps_1 = 10.0 + 10.0*1j
d1 = 0.05

Z1 = 1/sqrt(eps_1)

# one layer

eps_2 = 5.0 + 5.0*1j
d2 = 0.1

Z2 = 1/sqrt(eps_2)

Z3 = 1/sqrt(1.0)

Zin_2 = Z2*(Z3 - 1j*Z2*tan(2*pi/lam*sqrt(eps_2)*d2))/(Z2 - 1j*Z3*tan(2*pi/lam*sqrt(eps_2)*d2))

Zin_1 = Z1*(Zin_2 - 1j*Z1*tan(2*pi/lam*sqrt(eps_1)*d1))/(Z1 - 1j*Zin_2*tan(2*pi/lam*sqrt(eps_1)*d1))

V = (Zin_1 - Z0)/(Zin_1 + Z0)

print "two layers. reflected:", V*conj(V)




