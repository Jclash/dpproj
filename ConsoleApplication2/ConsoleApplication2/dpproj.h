#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;

#define _USE_MATH_DEFINES
#include <math.h>

#include <sstream>
#include <ctime>


#ifndef DPPROJ_DLL_H
#define DPPROJ_DLL_H

#ifdef BUILDING_DPPROJ_DLL
#define DPPROJ_DLL __declspec(dllexport)
#else
#define DPPROJ_DLL __declspec(dllimport)
#endif

#ifdef BUILDING_DPPROJ
#define DPPROJ_DLL " "
#endif


#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_cblas.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>


#define _USE_MATH_DEFINES
#include <math.h>

int dpproj_run(int argc, char **argv);
int load_data();
int calculation();
int output_data();

#define GO_MPI 0

#define GO_MNO 0

// TEXTOUT
// -1 - no output to stdou at all
// 0 - prints only total time
// 1 - output to stdout of system (basic) information
// 2 - output to stdout of the whole workflow
// 3 - output to stdout of the whole workflow and debug text
#define TEXTOUT 1

// FILEOUT
// 0 - no file output
// 1 - basic file output
// 2 - extended file output
// 3 - debug file output
#define FILEOUT 1

//int textout;
//bool layerout = true;

struct layer {
	double i1, i2, i3;
	double A1, A2, A3, B1, B2, B3, B0;
	double j1, j2, j3;
	double C1, C2, C3, D1, D2, D3, D0;
	double cylX, cylZ, cylR;
	double polB, polD, polA, betta;
    double a1, a2;
    double di, alphai;
	double epsilonk_re;
	double epsilonk_im;
};

struct type_paramsC0nj {
	double a; // period
	double alpha;
	double k0;

	// for type_paramsEps
	int layer_type;
	double z1; // start of layer
	double z2; // end of layer
	double A1; // sinus amplitude
	double w1; // sinus round frequency
	gsl_complex epsilon0;
	gsl_complex epsilon1;
	gsl_complex epsilon2;

	int n;
	int j;
	double z;

	layer fg; // functions defining the layer

	layer* fgs; // functions defining the layers
	gsl_complex* eps_k;
	int NL;

};


struct type_paramsEps{
	int layer_type;
	double z1; // start of layer
	double z2; // end of layer
	double A1; // sinus amplitude
	double w1; // sinus round frequency
	gsl_complex epsilon0;
	gsl_complex epsilon1;
	gsl_complex epsilon2;
	double a;
	layer fg; // functions defining the layer
	layer* fgs; // functions defining the layers
	gsl_complex* eps_k;
	int NL;
};

struct results{
	double D; // D
	double V; // V
	double def; // defect
	double R0; // defect
	gsl_complex* u; // function u
};

struct params{

	double lambda; // wavelength
	double a; // period
	double alpha; // angle of plane wave direction
	gsl_complex C0; // amplitude of the incedent wave
	double epsilon0_re; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	double epsilon0_im; // inductive capacity for vacuum - dielectricheskaja pronicaemost'
	double mu0; // magnitic inductivity for vacuum - magnitnaja pronicaemost'
	double zmin; // start of calculation domain
	double zmax; // end of calculation domain
	int polar; // polarization 1 - H-polarization
	           //              0 - E-polarization
    double zplus; // gap between the surfaces of real curved layer the rectangle where the computation is performed

	// \varepsilon(x,z)
	//int epsN; // type of periodic structure 1 - layer, 2 - sin
	double epsilon1_re; // inductive capacity for the periodic structure - real part
	double epsilon1_im; // inductive capacity for the periodic structure - complex part

	// layer type
	// type 1 - plane homogeneous layer
	// type 2 - [z1, z2 - A1 + A1*sin(w1*x)] homogeneous layer
	int layer_type;
	double z1; // start of layer
	double z2; // end of layer
	double A1; // sinus amplitude
	double w1; // sinus round frequency
	layer fg; // functions defining the layer

	int N1; // minimal n
	int N2; // maximal n
	int N; // number of n-s

	double w; // circular frequency - krugovaja chastota
	double zV; // boundary for sourses domain
	double Thetta; // angle of plane wave direction
	double A0; // plane wave amplitude
	double k; // wave number
	double k0; // wave number in domain V_III
	gsl_complex W; // Impedance
	double l; // Amplitude of the surface wave

	gsl_complex epsilon0;
	gsl_complex epsilon1;
	gsl_complex epsilon2;

	int Nx; // number of x values in [0,a]
	int Nz; // number of z values in [z_min,z_max]
	int Nm; // number of steps in of progonka
	double* zj;
	double* sigmaj;

	gsl_spline **Cnj_re, **Cnj_im;
    gsl_spline **C1nj_re, **C1nj_im;
	gsl_spline **D2nj_re, **D2nj_im;
	gsl_spline **C2nj_re, **C2nj_im;

	int calcnum = 0;
	int n;
	int s;
	double x;
	double t;
	double z;
	int m;
	//gsl_interp_type a_sn_spline;

	layer* fgs; // functions defining the layers
    int NL;
    gsl_complex* eps_k;

    char* infilename;
    char* outfilename;
};


int println_gsl_complex(gsl_complex);
int print_layer(params);
double absf(double);
int orthogonalization_norm_inplace( params, gsl_complex*, gsl_matrix_complex* );

int jac(double z, const double y[], double *dfdy, double dfdt[], void *pars);
int func(double z, const double y[], double f[], void *pars);
double lambda_n( int n, double k0, double a, double alpha );
double lambda_n( int n,params par );
double lambda_n( int n,type_paramsC0nj par );
gsl_complex gamma_n( int n, double k0, double a, double alpha );
gsl_complex gamma_n( int n, params par );
gsl_complex gamma_n( int n, type_paramsC0nj par );
int print_gsl_complex(gsl_complex a);
gsl_complex psi_n(double x, int n, void * pars);
int calculate_C1nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0);
int calculate_C2nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0);
int calculate_D2nj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0);
int calculate_Cnj(gsl_spline *** Cnj_re, gsl_spline *** Cnj_im, params* exp0);
int print_splineMatrix_toFile( gsl_spline** C1nj_re, gsl_spline** C1nj_im, double z1, double z2, int N, const char* mname);
int print_splineMatrix_toFile( gsl_spline** C1nj_re, gsl_spline** C1nj_im, double z1, double z2, int N, int n, int j, const char* mname);

int DPPROJ_DLL dll_run();
int DPPROJ_DLL load_parameters(string, params*);
int DPPROJ_DLL print_singleExp(std::ofstream& , params );
int DPPROJ_DLL dep_params_calc(params*);

#endif

