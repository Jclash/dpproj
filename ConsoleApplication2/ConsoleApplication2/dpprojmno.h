#pragma once

int calculation_MNO(params* exp0, results* res);

// int solve_SODE_MNO(params* exp0, int j, gsl_complex* Qsj_start, gsl_complex* Qsj_end);
// int solve_SODE_H_MNO( params* , int, gsl_complex*, gsl_complex* );

// int solve_SLE_MNO(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars);
// int solve_SLE_H_MNO(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars);


double complex_matrix_conditionalNumber_normA2(const gsl_matrix_complex* A, int N);

