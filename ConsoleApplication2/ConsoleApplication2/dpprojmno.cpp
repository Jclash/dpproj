
#include "stdafx.h"
#include "string.h"

#include "dpproj_lin.h"
#include "dpprojmno.h"


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
using namespace std;

#define _USE_MATH_DEFINES
#include <math.h>

#include <sstream>
#include <ctime>

//#include "mpi.h"

extern int textout;
extern bool layerout;



double complex_matrix_conditionalNumber_normA2(const gsl_matrix_complex* A, int N){

      gsl_matrix_complex* Acopy = gsl_matrix_complex_alloc (N, N);
      gsl_matrix_complex* m = gsl_matrix_complex_alloc (N, N);

      gsl_matrix_complex_memcpy ( Acopy, A );

	  gsl_blas_zgemm (CblasConjTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), A, Acopy, gsl_complex_rect(0.0, 0.0 ), m);

	  gsl_vector_complex* eval = gsl_vector_complex_alloc (2*N);
      gsl_matrix_complex* evec = gsl_matrix_complex_alloc (2*N, 2*N);

      gsl_eigen_nonsymmv_workspace* w = gsl_eigen_nonsymmv_alloc (2*N);

      gsl_matrix* mre = gsl_matrix_alloc(2 * N, 2 * N);
      for( int i = 0; i < N; i++){
        for( int j = 0; j < N; j++){
          gsl_matrix_set(mre, i, j, GSL_REAL(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i + N, j + N, GSL_REAL(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i, j + N,  GSL_IMAG(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i + N, j, -GSL_IMAG(gsl_matrix_complex_get(m,i,j)) );
        }
      }

      gsl_eigen_nonsymmv( mre, eval, evec, w );
      gsl_eigen_nonsymmv_free( w );
      gsl_eigen_nonsymmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC );

      gsl_complex eval_max = gsl_vector_complex_get (eval, 0);
      gsl_complex eval_min = gsl_vector_complex_get (eval, 2*N-1);

      if ( textout >= 3 ){
        printf("A2 eigenvalue_max = %g + %gi\n", GSL_REAL(eval_max), GSL_IMAG(eval_max));
        printf("A2 eigenvalue_min = %g + %gi\n", GSL_REAL(eval_min), GSL_IMAG(eval_min));
      }

      return sqrt(GSL_REAL(eval_max))/sqrt(GSL_REAL(eval_min));
}


int print_GrammaMatrix( gsl_matrix_complex* Qn, int N, int M){
  using namespace std;

  gsl_matrix_complex * matGramm = gsl_matrix_complex_alloc( M, M );

  gsl_complex sprod;
  gsl_vector_complex* vcol1 = gsl_vector_complex_alloc( N );
  gsl_vector_complex* vcol2 = gsl_vector_complex_alloc( N );
  for(int i = 0; i < M; i++){
    for(int k = 0; k < M; k++){
        gsl_matrix_complex_get_col( vcol1, Qn, i);
        gsl_matrix_complex_get_col( vcol2, Qn, k);
        gsl_blas_zdotc( vcol1, vcol2, &sprod );
        gsl_matrix_complex_set( matGramm, i, k, sprod );
    }
  }

  if (textout >= 3){
	printf("\nGramm matrix = \n");
	gsl_matrix_complex_fprintf(stdout, matGramm, "%g");
	//printf("\nb = \n");
	//gsl_vector_complex_fprintf(stdout, b, "%g");
  }
  return 0;
}



int orthogonalization_MNO_inplace( gsl_complex* Q, gsl_matrix_complex* Ws, double z, params par ){

  gsl_vector_complex** vecV;
  gsl_vector_complex** vecU;

  int N = par.N;

  gsl_matrix_complex * matQ = gsl_matrix_complex_alloc( 2*N, N );

  vecV = (gsl_vector_complex**) malloc(sizeof(gsl_vector_complex*)*N);
  vecU = (gsl_vector_complex**) malloc(sizeof(gsl_vector_complex*)*N);

  for(int i = 0; i < N; i++){
    vecV[i] = gsl_vector_complex_alloc( 2*N );
    vecU[i] = gsl_vector_complex_alloc( 2*N );
    for(int k = 0; k < 2*N; k++){
        gsl_vector_complex_set( vecV[i], k, Q[i*2*N + k] );
        gsl_matrix_complex_set( matQ, k, i, Q[i*2*N + k] );
    }
    //gsl_vector_complex_fprintf(stdout, vecV[i], "%g");
    //cout << endl;
  }


  //cout << endl << "Q matrix current:" << endl;
  print_GrammaMatrix( matQ, 2*N, N );
  //gsl_matrix_complex_fprintf(stdout, matQ, "%g");

  //
  // METHOD OF DIRECTED ORTOGONALIZATION
  //

  gsl_matrix_complex * A = gsl_matrix_complex_alloc( 2*N, 2*N );
  gsl_matrix_complex_set_zero( A ) ;

  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  for(int j = 0; j < N; j++) {

        // {Q_1,..Q_N}^T = (0|E)Q
		//f[j*2] = y[(j+N)*2];
		//f[j * 2+1] = y[(j + N) * 2+1];
    gsl_matrix_complex_set( A, j, j + N, gsl_complex_rect( 1.0, 0.0 ) );

		// {Q_N+1,..Q_2N}^T = (G|0)Q
	double re_temp = 0.0, im_temp = 0.0;
	for (int n = 0; n < N; n++) {
	  double CnjRE = gsl_spline_eval(par.Cnj_re[n*N + j], z, acc);
      double CnjIM = gsl_spline_eval(par.Cnj_im[n*N + j], z, acc);
			//re_temp += -CnjRE*y[n * 2] + CnjIM*y[n * 2 + 1];
			//im_temp += -CnjIM*y[n * 2] - CnjRE*y[n * 2 + 1];
      gsl_matrix_complex_set( A, j + N, j, gsl_complex_rect( -CnjRE, -CnjIM ) );
    }
		//f[(j+N) * 2] = re_temp;
		//f[(j + N) * 2 + 1] = im_temp;

  }

  gsl_interp_accel_free(acc);

  if (textout >= 4){
	printf("\nA(z_m) = \n");
	gsl_matrix_complex_fprintf(stdout, A, "%g");
  }


	  gsl_vector_complex* eval = gsl_vector_complex_alloc (4*N);
      gsl_matrix_complex* evec = gsl_matrix_complex_alloc (4*N, 4*N);

      gsl_eigen_nonsymmv_workspace* w = gsl_eigen_nonsymmv_alloc (4*N);

      gsl_matrix* mre = gsl_matrix_alloc(4 * N, 4 * N);
      for( int i = 0; i < 2*N; i++){
        for( int j = 0; j < 2*N; j++){
          gsl_matrix_set(mre, i, j, GSL_REAL(gsl_matrix_complex_get(A,i,j)) );
          gsl_matrix_set(mre, i + 2*N, j + 2*N, GSL_REAL(gsl_matrix_complex_get(A,i,j)) );
          gsl_matrix_set(mre, i, j + 2*N,  GSL_IMAG(gsl_matrix_complex_get(A,i,j)) );
          gsl_matrix_set(mre, i + 2*N, j, -GSL_IMAG(gsl_matrix_complex_get(A,i,j)) );
        }
      }

      if (textout >= 4){
	    printf("\nZ = \n");
	    gsl_matrix_fprintf(stdout, mre, "%g");
      }

      gsl_eigen_nonsymmv( mre, eval, evec, w );
      gsl_eigen_nonsymmv_free( w );
      gsl_eigen_nonsymmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC );

      if (textout >= 4){
        for (int i = 0; i < 4*N; i++){
          gsl_complex eval_i = gsl_vector_complex_get (eval, i);
          gsl_vector_complex_view evec_i = gsl_matrix_complex_column (evec, i);

          printf("eigenvalue = %g + %gi\n", GSL_REAL(eval_i), GSL_IMAG(eval_i));
          printf("eigenvector = \n");

          for (int j = 0; j < 4*N; ++j){
            gsl_complex z = gsl_vector_complex_get(&evec_i.vector, j);
            printf("%g + %gi\n", GSL_REAL(z), GSL_IMAG(z));
          }
          printf("=\n");
        }
      }
	  gsl_vector_complex* evalA = gsl_vector_complex_alloc (2*N);
      gsl_matrix_complex* evecA = gsl_matrix_complex_alloc (2*N, 2*N);

      for (int i = 0; i < 2*N; i++){
        gsl_complex eval_i1 = gsl_vector_complex_get (eval, 2*i);
        gsl_complex eval_i2 = gsl_vector_complex_get (eval, 2*i+1);
        gsl_vector_complex* evec_i1 = gsl_vector_complex_alloc (4*N);
        gsl_vector_complex* evec_i2 = gsl_vector_complex_alloc (4*N);
        gsl_vector_complex* veca = gsl_vector_complex_alloc (2*N);

        gsl_matrix_complex_get_col( evec_i1, evec, 2*i);
        gsl_matrix_complex_get_col( evec_i2, evec, 2*i+1);

        gsl_vector_complex_view evec_i1_UP = gsl_vector_complex_subvector (evec_i1, 0, 2*N);
        gsl_vector_complex_view evec_i1_BOT = gsl_vector_complex_subvector (evec_i1, 2*N, 2*N);
        gsl_vector_complex_view evec_i2_UP = gsl_vector_complex_subvector (evec_i2, 0, 2*N);
        gsl_vector_complex_view evec_i2_BOT = gsl_vector_complex_subvector (evec_i2, 2*N, 2*N);

        gsl_vector_complex* vcurr1 = gsl_vector_complex_alloc (2*N);
        gsl_vector_complex* vcurr2 = gsl_vector_complex_alloc (2*N);
        gsl_vector_complex* vcurr = gsl_vector_complex_alloc (2*N);


        gsl_vector_complex_memcpy( vcurr1, &evec_i1_UP.vector );
        // These functions compute the sum y = αx + y for the vectors x and y.
        gsl_blas_zaxpy(gsl_complex_rect(1.0,0.0), &evec_i2_UP.vector, vcurr1);
        gsl_blas_zdscal(0.5, vcurr1);

        gsl_vector_complex_memcpy( vcurr2, &evec_i1_BOT.vector );
        // These functions compute the sum y = αx + y for the vectors x and y.
        gsl_blas_zaxpy(gsl_complex_rect(1.0,0.0), &evec_i2_BOT.vector, vcurr2);
        gsl_blas_zscal(gsl_complex_rect(0.0,-0.5), vcurr2);

        gsl_vector_complex_memcpy( veca, vcurr1 );
        gsl_blas_zaxpy(gsl_complex_rect(1.0,0.0), vcurr2, veca);

        if (textout >= 4){
          printf("eigenvalue1 = %g + %gi\n", GSL_REAL(eval_i1), GSL_IMAG(eval_i1));
          printf("eigenvalue2 = %g + %gi\n", GSL_REAL(eval_i2), GSL_IMAG(eval_i2));
          printf("eigenvector a = \n");

          for (int j = 0; j < 2*N; ++j){
            gsl_complex z = gsl_vector_complex_get( veca, j);
            printf("%g + %gi\n", GSL_REAL(z), GSL_IMAG(z));
          }
          printf("=\n");
        }

        gsl_complex one = gsl_complex_rect(1.0,0.0);
        gsl_complex mone = gsl_complex_rect(-1.0,0.0);

        gsl_vector_complex_memcpy( vcurr1, veca );
        gsl_vector_complex_memcpy( vcurr2, veca );

        gsl_blas_zgemv (CblasNoTrans, one, A, veca, gsl_complex_mul(mone,eval_i1), vcurr1);
        gsl_blas_zgemv (CblasNoTrans, one, A, veca, gsl_complex_mul(mone,eval_i2), vcurr2);

        double norm1 = gsl_blas_dznrm2(vcurr1);
        double norm2 = gsl_blas_dznrm2(vcurr2);

        gsl_complex eval_A;
        if( norm1 < norm2){
          eval_A = eval_i1;
        }else{
          eval_A = eval_i2;
        }

        gsl_vector_complex_set(evalA, i, eval_A);
        gsl_matrix_complex_set_col(evecA, i, veca);

        if (textout >= 4){
          printf("eigenvalue_A = %g + %gi\n", GSL_REAL(eval_A), GSL_IMAG(eval_A));
        }


      }


     if (textout >= 4){
      cout << endl << "A(z) matrix:" << endl;
      for (int i = 0; i < 2*N; i++){
        gsl_complex eval_iA = gsl_vector_complex_get (evalA, i);
        gsl_vector_complex_view evec_iA = gsl_matrix_complex_column (evecA, i);

        printf("eigenvalue = %g + %gi\n", GSL_REAL(eval_iA), GSL_IMAG(eval_iA));
        printf("eigenvector = \n");

        for (int j = 0; j < 2*N; ++j){
          gsl_complex zA = gsl_vector_complex_get(&evec_iA.vector, j);
          printf("%g + %gi\n", GSL_REAL(zA), GSL_IMAG(zA));
        }
        printf("=\n");
      }

     }

  // DIRECTIONAL ORTHOGONALIZATION

  gsl_vector_complex* muPlusVal = gsl_vector_complex_alloc (N);
  gsl_matrix_complex* muPlusVecs = gsl_matrix_complex_alloc (2*N, N);

  gsl_vector_complex_view evecCur = gsl_matrix_complex_column (evecA, 0);

  int used[2*N];
  for(int k = 0; k < 2*N; k++) used[k] = 0;

  int idx;

  for( int i = 0; i < N; i++ ){
    idx = 0;
    while( used[idx] == 1){
      idx++;
    }
    gsl_complex evalCur = gsl_vector_complex_get (evalA, idx);
    evecCur = gsl_matrix_complex_column (evecA, idx);
    double maxRe = GSL_REAL(evalCur);
    int curj = idx;

    for( int j = 0; j < 2*N; j++){
      double checkVec = GSL_REAL(gsl_vector_complex_get (evalA, j));
      if( ( checkVec > maxRe ) && ( used[j] == 0 ) ){
        evalCur = gsl_vector_complex_get (evalA, j);
        evecCur = gsl_matrix_complex_column (evecA, j);
        maxRe = checkVec;
        curj = j;
      }
    }

    used[curj] = 1;
    gsl_vector_complex_set( muPlusVal, i, evalCur );
    gsl_matrix_complex_set_col( muPlusVecs, i, &evecCur.vector );

  }


     if (textout >= 3){
      if (textout >= 4) cout << endl << "A(z) matrix sorted:" << endl;
      for (int i = 0; i < N; i++){
        gsl_complex eval_iA = gsl_vector_complex_get (muPlusVal, i);
        gsl_vector_complex_view evec_iA = gsl_matrix_complex_column (muPlusVecs, i);

        printf("eigenvalue_sorted = %g + %gi\n", GSL_REAL(eval_iA), GSL_IMAG(eval_iA));
        if (textout >= 4) printf("eigenvector = \n");

        for (int j = 0; j < 2*N; ++j){
          gsl_complex zA = gsl_vector_complex_get(&evec_iA.vector, j);
          if (textout >= 4) printf("%g + %gi\n", GSL_REAL(zA), GSL_IMAG(zA));
        }
        if (textout >= 4) printf("=\n");
      }

     }

  //
  // Ws = ( muPlusVecs^T * matQ )^-1
  //

  gsl_matrix_complex_set_identity( Ws );

  gsl_matrix_complex* Dmat = gsl_matrix_complex_alloc (N, N);
  gsl_matrix_complex_set_zero( Dmat );

  //W = Wgsh*Wnorm
  gsl_blas_zgemm (CblasTrans, CblasNoTrans, gsl_complex_rect(1.0,0.0),
                   muPlusVecs, matQ, gsl_complex_rect(0.0,0.0), Dmat);

  // Dmat inversion

  gsl_permutation * p = gsl_permutation_alloc( N );
  int s;

  gsl_linalg_complex_LU_decomp(Dmat, p, &s);

  gsl_linalg_complex_LU_invert(Dmat, p, Ws);

  //
  // matQ = matQ * Ws
  //
  gsl_matrix_complex* Fmat = gsl_matrix_complex_alloc (2*N, N);
  gsl_matrix_complex_set_zero( Fmat );

  gsl_blas_zgemm (CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0,0.0),
                   matQ, Ws, gsl_complex_rect(0.0,0.0), Fmat);
  gsl_matrix_complex_memcpy( matQ, Fmat );


  //
  // NORMALIZATION
  //
  gsl_matrix_complex* Fmat1 = gsl_matrix_complex_alloc (N, N);
  gsl_complex temp;
  gsl_matrix_complex* Wss = gsl_matrix_complex_alloc (N, N);
  gsl_vector_complex* vcol = gsl_vector_complex_alloc( 2*N );;
  gsl_matrix_complex_set_zero( Wss );
  double cnorm;
  for(int i = 0; i < N; i++){
    gsl_matrix_complex_get_col( vcol, matQ, i);
    cnorm = gsl_blas_dznrm2( vcol );
    //gsl_complex temp = gsl_matrix_complex_get( Ws, i, i );
    //temp = gsl_complex_mul( gsl_complex_rect( 1.0/cnorm, 0.0 ), temp );
    temp = gsl_complex_rect( 1.0/cnorm, 0.0 );
    gsl_matrix_complex_set( Wss, i, i, temp );
  }

  gsl_matrix_complex_set_zero( Fmat1 );
  gsl_blas_zgemm (CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0,0.0),
                   Ws, Wss, gsl_complex_rect(0.0,0.0), Fmat1);
  gsl_matrix_complex_memcpy( Ws, Fmat1 );

  gsl_matrix_complex_set_zero( Fmat );
  gsl_blas_zgemm (CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0,0.0),
                   matQ, Wss, gsl_complex_rect(0.0,0.0), Fmat);
  gsl_matrix_complex_memcpy( matQ, Fmat );


  //gsl_matrix_complex_fprintf(stdout, matQ, "%g");
  print_GrammaMatrix( matQ, 2*N, N);

  for(int i = 0; i < N; i++){
    for(int k = 0; k < 2*N; k++){
        Q[i*2*N + k] = gsl_matrix_complex_get( matQ, k, i );
    }
  }

  return 0;
}


int orthogonalization_GrammShmidt_inplace( gsl_complex* Q, gsl_matrix_complex* Ws, int N, int M ){

  gsl_vector_complex** vecV;
  gsl_vector_complex** vecU;

  gsl_matrix_complex * matQ = gsl_matrix_complex_alloc( N, M );

  vecV = (gsl_vector_complex**) malloc(sizeof(gsl_vector_complex*)*M);
  vecU = (gsl_vector_complex**) malloc(sizeof(gsl_vector_complex*)*M);

  for(int i = 0; i < M; i++){
    vecV[i] = gsl_vector_complex_alloc( N );
    vecU[i] = gsl_vector_complex_alloc( N );
    for(int k = 0; k < N; k++){
        gsl_vector_complex_set( vecV[i], k, Q[i*N + k] );
        gsl_matrix_complex_set( matQ, k, i, Q[i*N + k] );
    }
    //gsl_vector_complex_fprintf(stdout, vecV[i], "%g");
    //cout << endl;
  }


  //cout << endl << "Q matrix current:" << endl;
  print_GrammaMatrix( matQ, N, M );
  //gsl_matrix_complex_fprintf(stdout, matQ, "%g");

  //
  // GRAMM_SHMIDT PROCESS
  //
  gsl_matrix_complex_set_identity( Ws );
  gsl_vector_complex * vecCur = gsl_vector_complex_alloc( N );
  gsl_matrix_complex * Wk = gsl_matrix_complex_alloc( M, M );
  gsl_matrix_complex * W = gsl_matrix_complex_alloc( M, M );
  gsl_matrix_complex * Wnorm = gsl_matrix_complex_alloc( M, M );
  gsl_matrix_complex * Wgsh = gsl_matrix_complex_alloc( M, M );
  gsl_matrix_complex_set_identity( Wgsh );
  gsl_matrix_complex_set_identity( Wnorm );
  gsl_matrix_complex_set_identity( W );
  gsl_matrix_complex_set_identity( Wk );
  for(int k = 0; k < M; k++){
    gsl_vector_complex_memcpy( vecU[k], vecV[k] );
    gsl_matrix_complex_set_identity( Wk );

    for(int j = 0; j <= k - 1; j++){

      gsl_complex mult;
      gsl_complex frac_up, frac_bot;

      // These functions compute the complex conjugate scalar product xHy for the vectors x and y, returning the result in dotc
      gsl_blas_zdotc ( vecU[j], vecV[k], &frac_up );
      gsl_blas_zdotc ( vecU[j], vecU[j], &frac_bot );
      mult = gsl_complex_mul(
                              gsl_complex_div( gsl_complex_rect(-1.0, 0.0), frac_bot),
                              frac_up
                              );

      gsl_vector_complex_memcpy( vecCur, vecU[j] );

      // This function multiplies the elements of vector a by the constant factor x.
      gsl_vector_complex_scale( vecCur, mult );


      // This function adds the elements of vector b to the elements of vector a.
      // he result a_i \leftarrow a_i + b_i is stored in a and b remains unchanged.
      gsl_vector_complex_add( vecU[k], vecCur );

      gsl_matrix_complex_set( Wk, j, k, mult );
    }

    //Wgsh = Wgsh*Wk
    gsl_blas_ztrmm (CblasRight, CblasUpper, CblasNoTrans, CblasNonUnit,
                    gsl_complex_rect(1.0,0.0), Wgsh, Wk);

  }

  for(int k = 0; k < M; k++){
    // NORMALIZATION
    gsl_complex length;
    gsl_blas_zdotc ( vecU[k], vecU[k], &length );
    length = gsl_complex_sqrt( length );
    gsl_vector_complex_scale( vecU[k], gsl_complex_inverse(length) );
    gsl_matrix_complex_set( Wnorm, k, k, gsl_complex_inverse(length) );
  }

  //W = Wgsh*Wnorm
  gsl_blas_zgemm (CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0,0.0),
                   Wgsh, Wnorm, gsl_complex_rect(0.0,0.0), W);

  gsl_matrix_complex_memcpy( Ws, W );


  for(int i = 0; i < M; i++){
    //gsl_matrix_set_col( matQ,, i, vecU[i] );
    for(int k = 0; k < N; k++){
        Q[i*N + k] = gsl_vector_complex_get( vecU[i], k );
        gsl_matrix_complex_set( matQ, k, i, Q[i*N + k] );
    }
  }

  //gsl_matrix_complex_fprintf(stdout, matQ, "%g");
  print_GrammaMatrix( matQ, N, M);

  return 0;
}


int orthogonalization_norm_inplace( params exp0, gsl_complex* Qsj_atzj, gsl_matrix_complex* Wss ){

  int N = exp0.N;

  gsl_matrix_complex * matQsj = gsl_matrix_complex_alloc( 2*N, N );
  gsl_matrix_complex * mattemp1 = gsl_matrix_complex_alloc( 2*N, N );

  for(int i = 0; i < 2*N; i++){
    for(int k = 0; k < N; k++){
      gsl_matrix_complex_set( matQsj, i, k, Qsj_atzj[ 2*N*k + i] );
    }
  }

  print_GrammaMatrix( matQsj, 2*N, N);

  gsl_vector_complex* vcol = gsl_vector_complex_alloc( 2*N );;
  double cnorm, mul;
  for(int i = 0; i < N; i++){
    for(int k = 0; k < N; k++){
      if ( i == k ){
        gsl_matrix_complex_get_col( vcol, matQsj, i);
        cnorm = gsl_blas_dznrm2( vcol );
        gsl_matrix_complex_set( Wss, i, k, gsl_complex_rect( 1.0/cnorm, 0.0 ) );
      }else{
        gsl_matrix_complex_set( Wss, i, k, gsl_complex_rect(0.0, 0.0 ) );
      }
    }
  }

  if (textout >= 3){
	printf("\nW[j+1] = \n");
	gsl_matrix_complex_fprintf(stdout, Wss, "%g");
	//printf("\nb = \n");
	//gsl_vector_complex_fprintf(stdout, b, "%g");
  }

  gsl_blas_zgemm(CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), matQsj, Wss, gsl_complex_rect(0.0, 0.0 ), mattemp1);

  for(int i = 0; i < 2*N; i++){
    for(int k = 0; k < N; k++){
      Qsj_atzj[ 2*N*k + i] = gsl_matrix_complex_get( mattemp1, i, k );
    }
  }

  print_GrammaMatrix( mattemp1, 2*N, N);

  return 0;
}

int orthogonalization_unity_inplace( params exp0, gsl_complex* Qsj_atzj, gsl_matrix_complex* Wss ){

  int N = exp0.N;

  gsl_matrix_complex * matQsj = gsl_matrix_complex_alloc( 2*N, N );
  gsl_matrix_complex * mattemp1 = gsl_matrix_complex_alloc( 2*N, N );

  for(int i = 0; i < 2*N; i++){
    for(int k = 0; k < N; k++){
      gsl_matrix_complex_set( matQsj, i, k, Qsj_atzj[ 2*N*k + i] );
    }
  }

  gsl_vector_complex* vcol = gsl_vector_complex_alloc( 2*N );;
  double mul;
  for(int i = 0; i < N; i++){
    for(int k = 0; k < N; k++){
      if ( i == k ){
        gsl_matrix_complex_get_col( vcol, matQsj, i);
        gsl_matrix_complex_set( Wss, i, k, gsl_complex_rect( 1.0, 0.0 ) );
      }else{
        gsl_matrix_complex_set( Wss, i, k, gsl_complex_rect(0.0, 0.0 ) );
      }
    }
  }

  if (textout >= 3){
	printf("\nW[j+1] = \n");
	gsl_matrix_complex_fprintf(stdout, Wss, "%g");
	//printf("\nb = \n");
	//gsl_vector_complex_fprintf(stdout, b, "%g");
  }

  gsl_blas_zgemm(CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), matQsj, Wss, gsl_complex_rect(0.0, 0.0 ), mattemp1);

  for(int i = 0; i < 2*N; i++){
    for(int k = 0; k < N; k++){
      Qsj_atzj[ 2*N*k + i] = gsl_matrix_complex_get( mattemp1, i, k );
    }
  }

  return 0;
}


int solve_SODE_H_MNO(params* exp0, int jj, gsl_complex* Qsj_start, gsl_complex* Qsj_end) {

	int N = (*exp0).N;
	int Nz = (*exp0).Nz;

    // array with all solution components Qj(z) for s-th initial values
    // QsjArr[Nz*j], ..., QsjArr[Nz*j+Nz-1] - values for function Qj

	//for(int j = 0; j < N * 2; j++)
	//  Qsj[j] = gsl_spline_alloc(gsl_interp_akima, Nz);

	double dz = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1);

	gsl_odeiv2_system sys = { func, jac, 2*N*2, exp0 };
	gsl_odeiv2_driver * d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, dz, 1e-6, 1e-6 );

	double z_start = (*exp0).zj[jj];
	double z_end = (*exp0).zj[jj + 1];

	double y[2*N*2];

	for (int i = 0; i < 2 * N; i++){
		y[i*2] =     GSL_REAL(Qsj_start[ i ]);
		y[i*2 + 1] = GSL_IMAG(Qsj_start[ i ]);
	}

	if(textout >= 3){
	  cout << "Applying driver...  j=" << jj << " s=unknown" << " z_start="<<z_start << " z_end="<< z_end;
	}

	int status = gsl_odeiv2_driver_apply(d, &z_start, z_end, y);

	if(textout >= 3){
	  cout << " done" << endl;
	}

	for(int j = 0; j < N * 2; j++) {
        Qsj_end[2*N*0 +j] = gsl_complex_rect(y[j * 2], y[j * 2 + 1]);
		//gsl_spline_init(Qsj[j], zarr, QsjArr+Nz*j, Nz);
	}

	return 0;
}



int solve_SLE_H_MNO(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars) {

	int N = pars.N;

	for (int i = 0; i < 2 * N; i++)
		alphas[i] = gsl_complex_rect(0.0, 0.0);

	gsl_matrix_complex * m = gsl_matrix_complex_alloc( N, N);
	gsl_vector_complex * b = gsl_vector_complex_alloc( N);
	gsl_vector_complex * x = gsl_vector_complex_alloc( N);

	for (int j = 0; j < N; j++) {
		gsl_vector_complex_set(b, j, gsl_complex_rect(0.0, 0.0));
		for (int s = 0; s < N; s++)
			gsl_matrix_complex_set(m, j, s, gsl_complex_rect(0.0, 0.0));
	}

	gsl_complex RP, Mjs;
	gsl_matrix_complex * JE = gsl_matrix_complex_alloc( N, 2*N);
	gsl_matrix_complex * Q = gsl_matrix_complex_alloc( 2*N, N);

	for (int j = 0; j < N; j++) {

		double alpha_j = lambda_n( j + pars.N1, pars );
		gsl_complex val_gamma_j = gamma_n( j + pars.N1, pars );

		//double alpha_j = pars.k0*sin(pars.alpha) + 2.0*M_PI/pars.a*(j + pars.N1);
		//double gamma_j = sqrt(pars.k0*pars.k0 - alpha_j*alpha_j);
        //gsl_complex val_gamma_j = gsl_complex_rect(gamma_j,0.0);

		if (textout >= 3){
			cout << endl << "gamma_j" << endl << ">>> " << GSL_REAL(val_gamma_j) << "+i*"<< GSL_IMAG(val_gamma_j) << endl << endl;
		}

		RP = gsl_complex_mul(
                gsl_complex_mul(val_gamma_j,gsl_complex_mul(gsl_complex_rect(0.0, -2.0*sqrt(pars.a)), pars.C0)),
                gsl_complex_exp(gsl_complex_mul(val_gamma_j,gsl_complex_rect(0.0, -pars.zmax)))
            );

		if (textout >= 3){
			cout << endl << "RP" << endl << ">>> ";  print_gsl_complex(RP); cout << endl << endl;
			cout << endl << "RP1" << endl << ">>> ";  print_gsl_complex(pars.C0); cout << endl << endl;
		}

		if (j + pars.N1 == 0)
			gsl_vector_complex_set(b, j, RP);

		//gsl_matrix_complex_set(m, j, j, gsl_complex_rect(0.0, gamma_j));
		//gsl_matrix_complex_set(m, j, j + N, gsl_complex_rect(1.0, 0.0));

		gsl_matrix_complex_set(JE, j, j, gsl_complex_mul(gsl_complex_rect(0.0, -1.0 ),val_gamma_j));
        gsl_matrix_complex_set(JE, j, N + j, gsl_complex_rect(1.0, 0.0 ));
        for(int n = 0; n < 2*N; n++){
            gsl_matrix_complex_set(Q, n, j, Qsj_zmax[ 2*N*j + n ]);
        }
	}


	gsl_blas_zgemm (CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), JE, Q, gsl_complex_rect(0.0, 0.0 ), m);

	if (textout >= 3){
		printf("m = \n");
		gsl_matrix_complex_fprintf(stdout, m, "%g");

		printf("\nb = \n");
		gsl_vector_complex_fprintf(stdout, b, "%g");
	}

    if (textout >= 3){

    /*
      gsl_matrix_complex * QR = gsl_matrix_complex_alloc(2 * N, 2 * N);
   	  gsl_vector_complex * tau = gsl_vector_complex_alloc(2 * N);
   	  gsl_vector_complex * norm = gsl_vector_complex_alloc(2 * N);
	  gsl_permutation * pp = gsl_permutation_alloc(2 * N);
	  int signum;

	  gsl_matrix_complex_memcpy(QR, m);

      gsl_linalg_complex_QRPT_decomp(QR, tau, pp, &signum, norm);

	  cout << "Condition number: " << endl;
	  */

	  gsl_vector_complex* eval = gsl_vector_complex_alloc (2*N);
      gsl_matrix_complex* evec = gsl_matrix_complex_alloc (2*N, 2*N);

      gsl_eigen_nonsymmv_workspace* w = gsl_eigen_nonsymmv_alloc (2*N);

      gsl_matrix* mre = gsl_matrix_alloc(2 * N, 2 * N);
      for( int i = 0; i < N; i++){
        for( int j = 0; j < N; j++){
          gsl_matrix_set(mre, i, j, GSL_REAL(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i + N, j + N, GSL_REAL(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i, j + N,  GSL_IMAG(gsl_matrix_complex_get(m,i,j)) );
          gsl_matrix_set(mre, i + N, j, -GSL_IMAG(gsl_matrix_complex_get(m,i,j)) );
        }
      }

      gsl_eigen_nonsymmv( mre, eval, evec, w );
      gsl_eigen_nonsymmv_free( w );
      gsl_eigen_nonsymmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC );

      {
      int i, j;

      for (i = 0; i < N; i++){
        gsl_complex eval_i = gsl_vector_complex_get (eval, i);
        gsl_vector_complex_view evec_i = gsl_matrix_complex_column (evec, i);

        printf("eigenvalue = %g + %gi\n", GSL_REAL(eval_i), GSL_IMAG(eval_i));
        printf("eigenvector = \n");

        for (j = 0; j < N; ++j){
          gsl_complex z = gsl_vector_complex_get(&evec_i.vector, j);
          printf("%g + %gi\n", GSL_REAL(z), GSL_IMAG(z));
        }
        printf("=\n");
      }
      }

      double condA;
      condA = complex_matrix_conditionalNumber_normA2(m, N);
      cout << endl << "cond(A) = " << condA << endl;

    }

    int s;

	gsl_permutation * p = gsl_permutation_alloc( N );

	gsl_linalg_complex_LU_decomp(m, p, &s);

	gsl_linalg_complex_LU_solve(m, p, b, x);

	if (textout >= 3){
		printf("\n alphas = \n");
		gsl_vector_complex_fprintf(stdout, x, "%g");
	}

	for (int i = 0; i < N; i++){
	    alphas[i] = gsl_vector_complex_get(x, i);
    }

	gsl_permutation_free(p);
	gsl_vector_complex_free(x);

	return 0;
}


int solve_SODE_MNO(params* exp0, int j, gsl_complex* Qsj_start, gsl_complex* Qsj_end){

	if ((*exp0).polar == 1){
            //cout << "H entered" << endl;
		return solve_SODE_H_MNO(exp0, j, Qsj_start, Qsj_end);
	}else{
        cout << "E entered" << endl;
        cout << "MNO for E-polarization is not yet implemented" << endl;
        return 1;
		//return solve_SODE_E(exp0, s, j, Qsj_start, Qsj_end);
	}
	return 1;
}


int solve_SLE_MNO(gsl_complex* alphas, gsl_complex* Qsj_zmax, params pars){

	if (pars.polar == 1){
		return solve_SLE_H_MNO( alphas, Qsj_zmax, pars);
	}else{
	    cout << "E entered" << endl;
        cout << "MNO for E-polarization is not yet implemented" << endl;
        return 1;
//		return solve_SLE_E( alphas, Qsj_zmax, pars);
	}
	return 1;
}



int calculation_MNO(params* exp0, results* res) {
  using namespace std;

  // STOP FOR E_POLARIZATION
  (*res).D = 0.0;
  (*res).V = 0.0;
  (*res).def = 0.0;
  if( (*exp0).polar != 1 ){
    return 0;
  }

  if(textout >= 2){
    cout<<"println_gsl_complex((*exp0).epsilon0);"<<endl<<">>> ";println_gsl_complex((*exp0).epsilon0);cout<<endl;
    cout<<"println_gsl_complex((*exp0).epsilon1);"<<endl<<">>> ";println_gsl_complex((*exp0).epsilon1);cout<<endl;
    cout<<"println_gsl_complex(psi_n(-0.5, 0, exp0));"<<endl<<">>> ";println_gsl_complex(psi_n(-0.5, 0, exp0));cout<<endl;
	cout << "N1" << endl << ">>> " << (*exp0).N1 << endl << endl;
	cout << "N2" << endl << ">>> " << (*exp0).N2 << endl << endl;
	cout << "N" << endl << ">>> " << (*exp0).N << endl << endl;
	cout << "layer_type" << endl << ">>> " << (*exp0).layer_type << endl << endl;
	cout << "i1" << endl << ">>> " << (*exp0).fg.i1 << endl << endl;
	cout << "A1" << endl << ">>> " << (*exp0).fg.A1 << endl << endl;
	cout << "a1" << endl << ">>> " << (*exp0).fg.a1 << endl << endl;
	cout << "a2" << endl << ">>> " << (*exp0).fg.a2 << endl << endl;
	cout << "zmin" << endl << ">>> " << (*exp0).zmin << endl << endl;
	cout << "zmax" << endl << ">>> " << (*exp0).zmax << endl << endl;
	cout << "lambda_n" << endl << ">>> " << lambda_n(0, *exp0) << endl << endl;
	cout << "polarisation" << endl << ">>> " << (*exp0).polar << endl << endl;
    if( (*exp0).layer_type == 7 ){
      cout << "di" << endl << ">>> " << (*exp0).fg.di << endl << endl;
	  cout << "alphai" << endl << ">>> " << (*exp0).fg.alphai << endl << endl;
      cout << "epsilon2_re" << endl << ">>> " << (*exp0).fg.epsilonk_re << endl << endl;
	  cout << "epsilon2_im" << endl << ">>> " << (*exp0).fg.epsilonk_im << endl << endl;
    }
  }

  int N = (*exp0).N;
  int Nz = (*exp0).Nz;
  int Nm = (*exp0).Nm;

  gsl_spline **Cnj_re, **Cnj_im;
  gsl_spline **C1nj_re, **C1nj_im;
  gsl_spline **D2nj_re, **D2nj_im;
  gsl_spline **C2nj_re, **C2nj_im;

  if( (*exp0).polar == 1 ){
    calculate_Cnj(&Cnj_re, &Cnj_im, exp0);
    (*exp0).Cnj_re = Cnj_re;
    (*exp0).Cnj_im = Cnj_im;
  }else{
	calculate_D2nj(&D2nj_re, &D2nj_im, exp0);
	(*exp0).D2nj_re = D2nj_re;
	(*exp0).D2nj_im = D2nj_im;
	calculate_C1nj(&C1nj_re, &C1nj_im, exp0);
	(*exp0).C1nj_re = C1nj_re;
	(*exp0).C1nj_im = C1nj_im;
	calculate_C2nj(&C2nj_re, &C2nj_im, exp0);
	(*exp0).C2nj_re = C2nj_re;
	(*exp0).C2nj_im = C2nj_im;
  }

  ofstream myfile;
  gsl_interp_accel *acc = gsl_interp_accel_alloc();

  if( FILEOUT >= 2){
  ////////////////////
  // TEST FOR C1nj(z) in the case of E-Polarization
    if( (*exp0).polar == 0 ){
      print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 0, 0, "C1");
      if( N >= 2 ) print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 1, 1, "C1");
      if( N >= 3 ) print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, 2, 2, "C1");

      print_splineMatrix_toFile( C1nj_re, C1nj_im, (*exp0).zmin, (*exp0).zmax, N, "C1");
      print_splineMatrix_toFile( C2nj_re, C2nj_im, (*exp0).zmin, (*exp0).zmax, N, "C2");
      print_splineMatrix_toFile( D2nj_re, D2nj_im, (*exp0).zmin, (*exp0).zmax, N, "D2");
    }
  ///////////////////

  ////////////////////
  // TEST FOR Cnj(z) in the case of H-Polarization
    if( (*exp0).polar == 1 ){
      print_splineMatrix_toFile( Cnj_re, Cnj_im, (*exp0).zmin, (*exp0).zmax, N, "C");
    }
  ///////////////////
  }

  //cout << "I am alive. PRE ode" << endl;

  // solve system of ODEs => Q1
  gsl_complex Qsj_zmax[2*N*2*N];
  //Qsj_zmax = (double*)malloc(sizeof(double)*(2 * N * 2 * N));
  //gsl_spline** Qsj;
  //Qsj = (gsl_spline**)malloc(sizeof(gsl_spline*)*(2*N*2*N));
  gsl_complex QsjArr[2*N*2*N *Nz];

  gsl_complex Qsj_atzj[N* 2*N *Nm];
  gsl_complex Ws[N*N *(Nm-1)];
  gsl_complex Ws_inv[N*N *(Nm-1)];

  // iterating through Nm intervals
  //
  // - initial conditions

  for(int s = 0; s < N; s++) {
    for(int j = 0; j < 2*N; j++){
      Qsj_atzj[ 2*N*N*0 + s*2*N + j] = gsl_complex_rect(0.0, 0.0);
    }
    //double alpha_s = (*exp0).k0*sin((*exp0).alpha) + 2.0*M_PI/(*exp0).a*(s + (*exp0).N1);
    //double gamma_s = sqrt((*exp0).k0*(*exp0).k0 - alpha_s*alpha_s);
	//
	//double alpha_s = lambda_n( s + pars.N1, pars );
    gsl_complex val_gamma_s = gamma_n( s + (*exp0).N1, *exp0 );

    Qsj_atzj[ 2*N*N*0 + s*2*N + s] = gsl_complex_rect(1.0, 0.0);
    Qsj_atzj[ 2*N*N*0 + s*2*N + s + N] = gsl_complex_mul(gsl_complex_rect(0.0,-1.0),val_gamma_s);
  }


  gsl_matrix_complex** Wss;
  Wss = (gsl_matrix_complex**) malloc(sizeof(gsl_matrix_complex*)*(Nm - 1));
  for(int i = 0; i < Nm - 1; i++){
    Wss[i] = gsl_matrix_complex_alloc( N, N );
  }

  for(int i = 0; i < N*N*(Nm-1); i++)
    Ws[i] = gsl_complex_rect(0.0, 0.0);
  for(int j = 0; j < Nm - 1; j++){
    for(int i = 0; i < N; i++){
      if( j == 0)
        Ws[N*N*j + i*N + i] = gsl_complex_rect(1.0, 0.0);
      else
        Ws[N*N*j + i*N + i] = gsl_complex_rect(1.0, 0.0);
    }
  }
  for(int j = 0; j < Nm - 1; j++){
    for(int i = 0; i < N; i++){
      for(int k = 0; k < N; k++){
        gsl_matrix_complex_set( Wss[j], i, k, Ws[N*N*j + i*N + k] );
      }
    }
  }

  for(int j = 0; j < Nm - 1; j++){
    // - solving SODE

    if(textout >= 3){
      cout << endl << "INTERVAL [z_j, z_j+1], j = " << j << endl << endl;

	  cout << "Qsj_atzj" << endl << ">>> ";
	  for (int ii = 0; ii < 2 * N; ii++){
            for (int ss = 0; ss < N; ss++){
			  print_gsl_complex( Qsj_atzj[ 2*N*N*(j) + ss*2*N + ii ] );
			  cout << " | ";
            }
            cout << endl;
	  }
	  cout << endl << endl;
    }

    for(int s = 0; s < N; s++) {

      solve_SODE_MNO( exp0, j, Qsj_atzj + 2*N*N*j + s*2*N, Qsj_atzj + 2*N*N*(j + 1) + s*2*N );

    }

    if(textout >= 3){
	  cout << "Qsj_atzj" << endl << ">>> ";
	  for (int ii = 0; ii < 2 * N; ii++){
            for (int ss = 0; ss < N; ss++){
			  print_gsl_complex( Qsj_atzj[ 2*N*N*(j+1) + ss*2*N + ii ] );
			  cout << " | ";
            }
            cout << endl;
	  }
	  cout << endl << endl;
    }

    // - Qsj_cur construction
    //Qsj_cur

    if ( j < Nm - 2 ){

      orthogonalization_MNO_inplace( Qsj_atzj + (2*N*N*(j + 1)), Wss[j+1], (*exp0).zj[j] , *exp0 );
      //orthogonalization_GrammShmidt_inplace( Qsj_atzj + (2*N*N*(j + 1)), Wss[j+1], 2*N, N );
      //orthogonalization_norm_inplace( *exp0, Qsj_atzj + (2*N*N*(j + 1)), Wss[j+1] );
      //orthogonalization_unity_inplace( *exp0, Qsj_atzj + (2*N*N*(j + 1)), Wss[j+1] );

    }


    // - saving Wj
  }

    // finding alpha_s coeffiecients
  gsl_complex alphas[ N * (Nm - 1) ];

  solve_SLE_MNO( alphas + N*(Nm - 2), Qsj_atzj + N*2*N*(Nm - 2) , *exp0 );

  // + constructing Bzmax from alphas and Qsj_cur

  gsl_complex Bzmax[N];

  for(int i = 0; i < N; i++){
    Bzmax[i] = gsl_complex_rect(0.0,0.0);
    for (int s = 0; s < N; s++) {
	  gsl_complex add_t1 = gsl_complex_mul( alphas[ N*(Nm - 2) + s ], Qsj_atzj[N*2*N*(Nm - 2) + 2*N*s + i]);
	  gsl_complex add_t2 = gsl_complex_add( Bzmax[i], add_t1 );
      Bzmax[i] = add_t2;
    }
  }

  gsl_complex Bzmin[N];

  gsl_matrix_complex * matWs_cur = gsl_matrix_complex_alloc( N, N);
  gsl_matrix_complex * matPw = gsl_matrix_complex_alloc( N, N);
  gsl_matrix_complex * mattemp = gsl_matrix_complex_alloc( N, N);
  gsl_matrix_complex * vecAlphas = gsl_matrix_complex_alloc( N,1 );

//  for (int i = 0; i < N; i++)
//	for (int j = 0; j < N; j++)
//	  gsl_matrix_complex_set(matWs_cur, i, j, Ws[ N*N*0 + i*N + j ] );

  gsl_matrix_complex_memcpy( matWs_cur, Wss[0]);
  gsl_matrix_complex_memcpy( matPw, matWs_cur);

    // finding inverse matrices Ws^-1 UNDER CONSTRUCTION!!!

  gsl_permutation * p = gsl_permutation_alloc (N);
  int s;
  gsl_matrix_complex** Wss_inv;
  Wss_inv = (gsl_matrix_complex**) malloc(sizeof(gsl_matrix_complex*)*(Nm - 1));
  for(int i = 0; i < Nm - 1; i++){
    Wss_inv[i] = gsl_matrix_complex_alloc( N, N );
    gsl_linalg_complex_LU_decomp (Wss[i], p, &s);
    gsl_linalg_complex_LU_invert (Wss[i], p, Wss_inv[i]);
  }

  for(int k = 0; k < (Nm-1); k++){
    for(int i = 0; i < N; i++){
      for(int j = 0; j < N; j++){
        Ws_inv[N*N*j + i*N + j] = gsl_matrix_complex_get(Wss_inv[k], i, j);
      }
    }
  }

  // Ws INVERSING
  //for(int i = 0; i < N; i++){
  //  Ws_inv[N*N*j + i*N + i] = gsl_complex_div (gsl_complex_rect( 1.0, 0.0 ), Ws[N*N*j + i*N + i]) ;
  //}


  for(int j = 1; j < Nm - 1; j++){

    for (int ii = 0; ii < N; ii++)
	  for (int jj = 0; jj < N; jj++)
	    gsl_matrix_complex_set(matWs_cur, ii, jj, gsl_matrix_complex_get( Wss[ j ], ii, jj ) );

	    //	    gsl_matrix_complex_set(matWs_cur, ii, jj, Ws_inv[ N*N*j + ii*N + jj ] );


    gsl_blas_zgemm(CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), matPw, matWs_cur, gsl_complex_rect(0.0, 0.0 ), mattemp);
    gsl_matrix_complex_memcpy( matPw, mattemp);

  }

  if(textout >= 3){
    cout << endl << "matrix mattemp:" << endl;
    gsl_matrix_complex_fprintf(stdout, mattemp, "%g");
    cout << endl;
  }

  for(int i = 0; i < N; i++)
    gsl_matrix_complex_set(vecAlphas, i, 0, alphas[ N*(Nm - 2) + i ]);

  gsl_matrix_complex * vecAlphasZmin = gsl_matrix_complex_alloc( N, 1 );
  gsl_blas_zgemm(CblasNoTrans, CblasNoTrans, gsl_complex_rect(1.0, 0.0 ), matPw, vecAlphas, gsl_complex_rect(0.0, 0.0 ), vecAlphasZmin);

  if(textout >= 3){
    cout << endl << "vecAlphas:" << endl;
    gsl_matrix_complex_fprintf(stdout, vecAlphas, "%g");
    cout << endl;

    cout << endl << "vecAlphasZmin:" << endl;
    gsl_matrix_complex_fprintf(stdout, vecAlphasZmin, "%g");
  }

  for(int i = 0; i < N; i++){
    Bzmin[i] = gsl_complex_rect(0.0,0.0);
    for (int s = 0; s < N; s++) {
      gsl_complex add_t1 = gsl_complex_mul(gsl_matrix_complex_get(vecAlphasZmin, s, 0), Qsj_atzj[ 2*N*N*0 + i*2*N + s]);
      gsl_complex add_t2 = gsl_complex_add(Bzmin[i], add_t1);
      Bzmin[i] = add_t2;
    }
  }

  int N1 = (*exp0).N1;
  int N2 = (*exp0).N2;
  int Nx = (*exp0).Nx;

  if (textout >= 2){
    cout << endl << "B[z_max]:" << endl;
    for (int j = 0; j < N; j++) {
        cout << GSL_REAL(Bzmax[j]) << " + " << GSL_IMAG(Bzmax[j]) << "*i" << endl;
        cout << sqrt(GSL_REAL(Bzmax[j])*GSL_REAL(Bzmax[j]) + GSL_IMAG(Bzmax[j])*GSL_IMAG(Bzmax[j])) << endl << endl;
    }
    cout << endl;

    cout << "B[z_min]:" << endl;
    for (int j = 0; j < N; j++) {
        cout << GSL_REAL(Bzmin[j]) << " + " << GSL_IMAG(Bzmin[j]) << "*i" << endl;
    }
    cout << endl;
  }


  // Calculating V
  double D = 0.0, V = 0.0, def = 0.0;

  for (int j = 0; j < N; j++) {
      D += gsl_complex_abs( Bzmin[j] )*gsl_complex_abs( Bzmin[j] );
	  if (j + (*exp0).N1 != 0)
		  V += gsl_complex_abs(Bzmax[j])*gsl_complex_abs(Bzmax[j]);
	  else {
		  gsl_complex u0 = gsl_complex_mul(gsl_complex_rect(sqrt((*exp0).a), 0.0), gsl_complex_mul((*exp0).C0, gsl_complex_exp(gsl_complex_rect(0.0, -(*exp0).k0*cos((*exp0).alpha)*(*exp0).zmax))));
		  V += gsl_complex_abs(gsl_complex_sub(Bzmax[j], u0))*gsl_complex_abs(gsl_complex_sub(Bzmax[j], u0));
	  }
  }
  D /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  V /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  def = 1 - D - V;

  if (textout >= 2){
	  cout << "V (new MNO) " << endl << ">>> " << V << endl << endl;
  }

  (*res).D = D;
  (*res).V = V;
  (*res).def = def;

    // epsilon
  if ( layerout && ( FILEOUT >= 2) ){
	  print_layer( *exp0 );
  }


return 0;


  if (textout >= 2) {
	  cout << "Qsj_zmax" << endl << ">>> ";
	  for (int s = 0 ; s < 2 * N; s++) {
		  cout << "s = " << s << ":" << endl;
		  for (int i = 0; i < 2 * N; i++)
			  println_gsl_complex(Qsj_zmax[i+s*2*N]);
			  cout << " ";
		  cout << endl;
	  }
	  //cout << endl;
  }

  //cout << "I am alive." << endl;


  if(textout >= 2){
	  cout << "alpha[s]" << endl << ">>> ";
	  for (int i = 0; i < 2 * N; i++){
			  print_gsl_complex(alphas[i]);
			  cout << " ";
	  }
	  cout << endl << endl;
  }

/*
  // Constructing B
  gsl_complex B[N * Nz];
  for (int j = 0; j < N; j++) {
	  for (int i = 0; i < Nz; i++) {
		  B[j * Nz + i] = gsl_complex_rect(0.0,0.0);
		  for (int s = 0; s < 2 * N; s++) {
			  gsl_complex add_t1 = gsl_complex_mul(alphas[s], QsjArr[s * 2 * N*Nz + Nz*j + i]);
			  gsl_complex add_t2 = gsl_complex_add(B[j * Nz + i], add_t1);
			  B[j * Nz + i] = add_t2;
		  }
	  }
  }
*/

/*
  if( FILEOUT >= 2){
  if ((*exp0).calcnum == 0) {
	  ofstream myfile1;
	  // B0
	  myfile1.open("B0_abs.txt");
	  for (int i = 0; i < Nz; i++) {
		  double z = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*i + (*exp0).zmin;
		  myfile1 << z << " " << gsl_complex_abs(B[(-N1)*Nz + i]) << endl;
	  }
	  myfile1.close();
	  // B0
	  myfile1.open("B0_arg.txt");
	  for (int i = 0; i < Nz; i++) {
		  double z = ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*i + (*exp0).zmin;
		  myfile1 << z << " " << gsl_complex_arg(B[(-N1) * Nz + i]) << endl;
	  }
	  myfile1.close();

  }
  }

  */

  // epsilon
  if ( layerout && ( FILEOUT >= 2) ){
	  print_layer(*exp0);
  }

/*
  // Calculating V, D, def
  double D = 0.0, V = 0.0, def;

  for (int j = 0; j < N; j++) {
	  D += gsl_complex_abs(B[j*Nz + 0])*gsl_complex_abs(B[j*Nz + 0]);
	  if (j + (*exp0).N1 != 0)
		  V += gsl_complex_abs(B[j*Nz + Nz - 1])*gsl_complex_abs(B[j*Nz + Nz - 1]);
	  else {
		  gsl_complex u0 = gsl_complex_mul(gsl_complex_rect(sqrt((*exp0).a), 0.0), gsl_complex_mul((*exp0).C0, gsl_complex_exp(gsl_complex_rect(0.0, -(*exp0).k0*cos((*exp0).alpha)*(*exp0).zmax))));
		  V += gsl_complex_abs(gsl_complex_sub(B[j*Nz + Nz - 1], u0))*gsl_complex_abs(gsl_complex_sub(B[j*Nz + Nz - 1], u0));
	  }
  }
  D /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  V /= gsl_complex_abs((*exp0).C0)*gsl_complex_abs((*exp0).C0)*(*exp0).a;
  def = 1 - D - V;

  if (textout >= 2){
	  cout << "D" << endl << ">>> " << D << endl << endl;
	  cout << "V" << endl << ">>> " << V << endl << endl;
	  cout << "def" << endl << ">>> " << def << endl << endl;
  }
*/

  // Constructing solution u(x,z)
  // on [0,a]x[z_min,z_max]
  //
  //int Nx = (*exp0).Nx;
  //gsl_complex u[Nx * Nz];

  /*
  gsl_complex * u = (*res).u;
  double x, z;
  type_paramsC0nj parC0nj;
  init_paramsC0nj(&parC0nj, *exp0);
  for (int ix = 0; ix < Nx; ix++) {
	  for (int iz = 0; iz < Nz; iz++) {
		  u[ix * Nz + iz] = gsl_complex_rect(0.0,0.0);
		  x = (*exp0).a / (Nx - 1)*ix;
		  z = (*exp0).zmin + ((*exp0).zmax - (*exp0).zmin) / (Nz - 1)*iz;
		  for (int j = 0; j < N; j++) {
			  gsl_complex add_t1 = gsl_complex_mul(B[j * Nz + iz], psi_n(x, j, &parC0nj));
			  gsl_complex add_t2 = gsl_complex_add(u[ix * Nz + iz], add_t1);
			  u[ix * Nz + iz] = add_t2;
		  }
	  }
  }*/

  (*res).D = D;
  (*res).V = V;
  (*res).def = def;

  //(*res).u = u;
  /*
  (*res).u = (gsl_complex*)malloc(sizeof(gsl_complex)*Nx * Nz);
  for (int i = 0; i < Nx*Nz; i++) {
	  ((*res).u)[i] = u[i];
  }
  */

  return 0;
}

