#!/bin/bash
> iwascreated.txt

S1='mvs100k'
S2='mvs10p'
S3='mvs10p-broadwell'
S10='hydra'
S11='hydra-gpu'
S0='unknown'

echo "Command line parameter recieved:" $1
echo "Available options:" $S1 $S2 $S10 $S11


if [ $1 = $S1 ] 
then 
  
  echo "Runs on MSC..."
  Sx=$S1

else
  
  if [ -f ./cluster-hydra ]; then

    echo "Hybrilit cluster detected"
    Sx=$S10
	    
  elif [ -f ./cluster-hydra-gpu ]; then
	    
    echo "Hybrilit cluster detected with GPU"	
    echo "Sorry, GPU execution is not implemented"
    Sx=S11
	    	    
  elif [ -f ./cluster-mvs100k ]; then
	    	    
    echo "JSCC RAS MVS-100K cluster detected"
    Sx=$S1
	    	            
  elif [ -f ./cluster-mvs10p ]; then
	    	            
    echo "JSCC MVS-10P cluster detected"
    Sx=$S2

  elif [ -f ./cluster-mvs10p-broadwell ]; then
	    	            
    echo "JSCC MVS-10P BROADWELL cluster detected"
    Sx=$S3

	    	                    
  else
	    	                    
    echo "Unknown computational system"
    Sx=$S0
	    	                            
  fi	    	                            
  
fi

echo Program runs on $Sx



######################### 
# common prerun actions #
#########################

rm -f ./output/*
if [ -f ./output/run.1 ]; then
  rm -f ./output/run.1/*
  rm -rf ./output/run.1
fi

cp ./bin/run ./output/run
cp ./input.txt ./output/input.txt


############################## 
# platform dependent actions #
##############################

if [ $Sx = $S1 ] 
then 

  echo "Initializing start on MVS-100K..."
  
  cp ./start_script-mpirun ./output/start_script-mpirun
  cp ./start_msc ./output/start_msc

  cd output
  ./start_msc
  
elif [ $Sx = $S3 ]; then
	    
  echo "Initializing starts on MVS-10P Broadwell"	

  cp ./start_mvs10p-broadwell ./output/start_mvs10p-broadwell
  
  cp ./nodes ./output/nodes
  
  cp ./start_script-mpirun ./output/start_script-mpirun

  cd output
  ./start_mvs10p-broadwell

elif [ $Sx = $S10 ]; then
	    
  echo "Initializing starts on HybriLIT"	

  cp ./start_hydra ./output/start_hydra
  
  cp ./nodes ./output/nodes
  
  cp ./start_script ./output/start_script

  cd output
  ./start_hydra


else

  echo "Runs on undefined system..."

    rm -f ./output/*

    cp ./bin/run ./output/run
    cp ./input.txt ./output/input.txt

    cd output
    ./run

    cp ./output.txt ../output.txt  
  
fi
 

########################## 
# common afterun actions #
##########################

cp ./output.txt ../output.txt
  

