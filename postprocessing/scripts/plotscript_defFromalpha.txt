#set terminal png  size 1600,1200 enhanced font 'Verdana,34'
#set output 'figDefFromAlpha.png'

set terminal epscairo
set output 'figDefFromAlpha.eps'


set xlabel "alpha" font "Arial,18"
set ylabel "d" font "Arial,18"
#set yrange [0:0.5]
set style line 1 lt 1 pt 7
#set xrange [ 0 : 1.0 ]
#set xtics 0.1
set key left bottom
plot "output_defFromAlpha_cyl.txt" title "cylinder" with linespoints linestyle 1 lw 3, "output_defFromAlpha_nocyl.txt" title "no cylinder" with linespoints linestyle 2 lw 3
