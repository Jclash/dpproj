
import math, cmath
from numpy import *

lam = 2*pi

eps_0 = 1.0

Z0 = 1/sqrt(eps_0)

# two media

eps_1 = 10.0 + 10.0*1j
d1 = 2.05

Z1 = 1/sqrt(eps_1)

V = (Z1 - Z0)/(Z1 + Z0)

print "two media. reflected:", V*conj(V)

# one layer

eps_2 = 5.0 + 5.0*1j
d2 = 4.0

Z2 = 1/sqrt(eps_2)

Zin = Z1*(Z2 - 1j*Z1*tan(2*pi/lam*sqrt(eps_1)*d1))/(Z1 - 1j*Z2*tan(2*pi/lam*sqrt(eps_1)*d1))

V = (Zin - Z0)/(Zin + Z0)

print "two media. reflected:", V*conj(V)




