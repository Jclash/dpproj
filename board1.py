import math, cmath
from numpy import *

#eps = 42.0 + 115.0*1j
eps = 42.0 + 115.0*1j


alpha = 50.0*pi/180.0

a0 = cos(alpha)
b0 = sqrt( eps - sin(alpha)*sin(alpha) )

# reflection coefficient when the E vector belongs to the incedense plane
# E-polarization
r2 = (eps*a0-b0)/(eps*a0+b0)

print "E-pol:"
print "d_E:", 1-r2*conj(r2)
print "D_E:", 0
print "V_E:", r2*conj(r2)

# reflection coefficient when the E vector is orthogonal to the incedense plane
# H-polarization
r1 = (a0-b0)/(a0+b0)

print "H-pol:"
print "d_H:", 1-r1*conj(r1)
print "D_H:", 0
print "V_H:", r1*conj(r1)

exit(0)

print 3/25.0
print 4/25.0
print -22/25.0

zmin = -0.1
z = 0.0
Q2 = 1/(2.0*exp(sqrt(eps)*1j*zmin))*exp(sqrt(eps)*1j*z) + 1/(2.0*exp(-sqrt(eps)*1j*zmin))*exp(-sqrt(eps)*1j*z)
Q5 = 1.0/eps*( sqrt(eps)*1j/(2.0*exp(sqrt(eps)*1j*zmin))*exp(sqrt(eps)*1j*z) - sqrt(eps)*1j/(2.0*exp(-sqrt(eps)*1j*zmin))*exp(-sqrt(eps)*1j*z) )
print "Q2 = ", Q2
print "Q5 = ", Q5

print "NEW SOLUTION"

z = -0.1
epsilon = 1.0
r1 = 1.0
r2 = 0.0
C1z = (sqrt(epsilon)*1j*r1 - r2)/(2.0*sqrt(epsilon)*1j)*exp(sqrt(epsilon)*1j*z)
C2z = (sqrt(epsilon)*1j*r1 + r2)/(2.0*sqrt(epsilon)*1j)*exp(-sqrt(epsilon)*1j*z)
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p 
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5

z = 0.0
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1.0/epsilon*Q2p  
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5


z = 0.0
epsilon = eps
r1 = Q2
r2 = eps*Q2p
C1z = (sqrt(epsilon)*1j*r1 - r2)/(2.0*sqrt(epsilon)*1j)*exp(sqrt(epsilon)*1j*z)
C2z = (sqrt(epsilon)*1j*r1 + r2)/(2.0*sqrt(epsilon)*1j)*exp(-sqrt(epsilon)*1j*z)
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1.0/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5

z = 10.0
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5


z = 10.0
epsilon = 1.0
r1 = Q2
r2 = 1/eps*Q2p
C1z = (sqrt(epsilon)*1j*r1 - r2)/(2.0*sqrt(epsilon)*1j)*exp(sqrt(epsilon)*1j*z)
C2z = (sqrt(epsilon)*1j*r1 + r2)/(2.0*sqrt(epsilon)*1j)*exp(-sqrt(epsilon)*1j*z)
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5

z = 10.1
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5



print "NEW SOLUTION Q1, Q4"

z = -0.1
epsilon = 1.0
r1 = 1.0
r2 = 0.0
Q1 = r1
Q1p = 0.0
Q4 = 0.0*Q1p 
print "Q1(", z, ") = ", Q1
print "Q4(", z, ") = ", Q4

z = 0.0
Q1 = r1
Q1p = 0.0
Q4 = 0.0
print "Q1(", z, ") = ", Q1
print "Q4(", z, ") = ", Q4


z = 0.0
epsilon = eps - 1.0
r1 = Q1
r2 = Q1p
C1z = (sqrt(epsilon)*1j*r1 - r2)/(2.0*sqrt(epsilon)*1j)*exp(sqrt(epsilon)*1j*z)
C2z = (sqrt(epsilon)*1j*r1 + r2)/(2.0*sqrt(epsilon)*1j)*exp(-sqrt(epsilon)*1j*z)
Q1 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q1p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q4 = 1/eps*Q1p
print "Q1(", z, ") = ", Q1
print "Q4(", z, ") = ", Q4

z = 10.0
Q1 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q1p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q4 = 1/eps*Q1p
print "Q1(", z, ") = ", Q1
print "Q4(", z, ") = ", Q4


z = 10.0
epsilon = 0.0
r1 = Q2
r2 = Q2p
C1z = (sqrt(epsilon)*1j*r1 - r2)/(2.0*sqrt(epsilon)*1j)*exp(sqrt(epsilon)*1j*z)
C2z = (sqrt(epsilon)*1j*r1 + r2)/(2.0*sqrt(epsilon)*1j)*exp(-sqrt(epsilon)*1j*z)
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5

z = 10.1
Q2 = C1z*exp(-sqrt(epsilon)*1j*z) + C2z*exp(sqrt(epsilon)*1j*z)
Q2p = -sqrt(epsilon)*1j*C1z*exp(-sqrt(epsilon)*1j*z) + sqrt(epsilon)*1j*C2z*exp(sqrt(epsilon)*1j*z)
Q5 = 1/epsilon*Q2p
print "Q2(", z, ") = ", Q2
print "Q5(", z, ") = ", Q5

