import math, cmath
import os, numpy
import sys
import time


# runos="windows"
# OR
# runos="linux"
runos="windows"

B0 = 2.5
layers = [ [1.0, 3.0, 1.0], [1.2, 2.8, 1.1], [1.1, 3.1, 0.7], [1.4, 2.5, 0.9], [1.4, 4.0, 1.5], [1.2, 3.1, 0.5], [1.4, 1.6, 0.8], [1.4, 3.0, 1.2], [1.2, 2.6, 0.8] ]
NL = len(layers)

A1_start = 0.2
A1_end = 0.2
A1_num = 1

A1_range=[]
A1_range.append(A1_start)
for i in range(1,A1_num):
  A1_range.append( A1_start + i*(A1_end - A1_start)/(A1_num - 1) )   

A2_start = 0.0
A3_start = 0.0
A23delta = 0.5
Ndist = 0

Lambda_1 = 2*math.pi
Lambda_2 = 2*Lambda_1
Lambda_3 = 0.5*Lambda_1

b = 2*Lambda_1

i1 = b/Lambda_1
i2 = b/Lambda_2
i3 = b/Lambda_3

j1 = i1
j2 = i2
j3 = i3

lam = math.pi

H = 'H'
E = 'E'

sysargs = sys.argv[1:]

clpars = "i"
if sysargs != []:
  clpars = (sys.argv[1:])[0]

print clpars


exp = { 
  'NL': 1, 
  'B0': 0.0,  'A1': A1_start, 'i1': i1,  'A2': 0.0, 'i2': i2,  'A3': 0.0, 'i3': i3,          'B1': 0.0, 'B2': 0.0, 'B3': 0.0, 
  'D0': -3.0, 'C1': 0.0, 'j1': j1, 'C2': 0.0, 'j2': j2, 'C3': 0.0, 'j3': j3,         'D1': 0.0, 'D2': 0.0, 'D3': 0.0,  
  'b': b, 'pol': H, 'N': 100, 'M': 100, 'alpha': 30.0, 'eps_re': 42.0, 'eps_im': 115.0, 'lam': lam,  'layer_type': 9
}


####################
# DATA PREPARATION #
####################

if "i" in clpars:

  print "Basic parameters:\n", exp, "\n"

  handle = open("input.txt", "w")

  print "Constructing experiments..."

  dL = ( exp['B0'] - exp['D0'] ) / NL
          
  print dL

  exp['NL'] = NL

  exp[ 'B0' ] = B0
  curPos = B0
#
# Constructing interfaces between layers
#
  for n in range( NL - 1 ):
    k = n + 1

    curPos = curPos - layers[n][0]
    parstr = 'B0_' + str(k)
    exp[ parstr ] = curPos

    parstr = 'A1_' + str(k)
    exp[ parstr ] = exp[ 'A1' ]

    parstr = 'i1_' + str(k)
    exp[ parstr ] = exp[ 'i1' ]

    parstr = 'i2_' + str(k)
    exp[ parstr ] = exp[ 'i2' ]

    parstr = 'i3_' + str(k)
    exp[ parstr ] = exp[ 'i3' ]


  exp[ 'D0' ] = curPos - layers[NL-1][0]
  exp[ 'C1' ] = exp[ 'A1' ]



#
# Defining epsilons for layers
#
  for n in range( NL ):
    k = n + 1

    parstr = 'eps' + str(k) + '_re'
    exp[ parstr ] = layers[n][1]

    parstr = 'eps' + str(k) + '_im'
    exp[ parstr ] = layers[n][2]
     
#
# Gauss
#

  A2_ranges=[]
  A3_ranges=[]

  A2_cur=[]
  A3_cur=[]
  for n in range( NL - 1 ):
    A2_cur.append( A2_start )
    A3_cur.append( A3_start )
  A2_ranges.append( A2_cur )
  A3_ranges.append( A3_cur )  
    
  for i in range( Ndist ):
    A2_cur=[]
    A3_cur=[]
    for n in range( NL - 1 ):
      A2_cur.append( numpy.random.normal( A2_start, A23delta, None) )
      A3_cur.append( numpy.random.normal( A3_start, A23delta, None) )
    A2_ranges.append( A2_cur )
    A3_ranges.append( A3_cur )  

  print A2_ranges

  for A2,A3 in zip( A2_ranges, A3_ranges ):
    for A1 in A1_range:
      exp['A1'] = A1

      for n in range( NL - 1 ):
        k = n + 1

        parstr = 'A2_' + str(k)
        exp[ parstr ] = A2[n]

        parstr = 'A3_' + str(k)
        exp[ parstr ] = A3[n]

      exp_str = str( exp )+'\n'
      exp_str = exp_str.replace( "'H'", "H" )
      exp_str = exp_str.replace( "'E'", "E" )
  
      print exp_str
      handle.write( exp_str )

  handle.close()



#sys.exit()


########################
# RUNNING COMPUTATIONS #
########################

if "c" in clpars:
  print "Starting computations.."
  os.system("./rs")
 
 
###########
# WAITING #
###########

if "w" in clpars:
  print "Waiting for computations to complete..."
  
  pathstr = "./output/output.txt"
  
  exists = os.path.isfile( pathstr )
  while not exists:
    time.sleep(1)
    exists = os.path.isfile( pathstr )
  


##################
# POSTPROCESSING #
##################

if "p" in clpars:

  es=[]
  level=[]
  
  if runos == "linux":
    os.system( "cp ./output/output.txt ./postprocessing/" )
  else:
    os.system( "copy .\\output\\output.txt .\\postprocessing\\" )

  Nexps = 0
  with open('./postprocessing/output.txt', 'r') as f:
    print 'Loaded input data:'
    for y in [x.rstrip() for x in f.readlines()]:
      print y
      y = y.replace( 'nan', '-100', 4 )
      t = eval(y) 
      es.append( t ) 
      level.append(t['defect'])
      Nexps = Nexps + 1
      print t

  print level

  n = 1
  while n*A1_num <= Nexps:
    with open('./postprocessing/graph'+str(n)+'.txt', 'w') as f:  
      print 'Constructing curve ' + str(n)
      for i in range(A1_num):
        if level[ i + (n-1)*A1_num ] >= 0.0 and level[ i + (n-1)*A1_num ] <= 1.0:
          #print A1_range[ i ], deffs[ i + (n-1)*A1_num ]
          f.write( str( A1_range[ i ] ) + " " + str( level[ i + (n-1)*A1_num ]) + '\n' )
      #if R0[ A1_num-1 + (n-1)*A1_num ] < 0.5:
        #print n
    n = n + 1
  Ngraphs = n - 1
    

  gp_base_script_name = "plotscript_defFromA1_01.txt"
  gp_script_name = "plotscript_defFromA1.txt"

  if runos == "linux":
    os.system( "cp ./postprocessing/scripts/"+gp_base_script_name+" ./postprocessing/"+gp_script_name )
  else:
    os.system( "copy .\\postprocessing\\scripts\\"+gp_base_script_name+" .\\postprocessing\\"+gp_script_name )
  
  plotstr = "plot "
  
  with open('./postprocessing/'+gp_script_name, 'a') as f:  
    for n in range( Ngraphs - 1 ):
      plotstr = plotstr + "'graph"+str(n+2)+".txt' notitle with lp lt 0 pt 12 lc rgb 'gray' lw 1 ps 1, "
    plotstr = plotstr + " 'graph1.txt' title 'A_2, A_3 = 0' with lp pt 20 lw 2 lc rgb 'red' ps 2"
#    plotstr = plotstr + ", 'graph1.txt' notitle with p pt 19 pc rgb 'black' ps 2"
    #print plotstr
    f.write( plotstr )
  
  if runos == "linux":
    os.system( "cd ./postprocessing; gnuplot plotscript_R0FromA1.txt" )    
  else:
    os.system( "cd .\\postprocessing; gnuplot plotscript_R0FromA1.txt" )    
  
  


