import math, cmath
import os, numpy
import sys
import time


runos="linux"

A1_start = 0.0
A1_end = 0.5
A1_num = 8

A1_range=[]
A1_range.append(A1_start)
for i in range(1,A1_num):
  A1_range.append( A1_start + i*(A1_end - A1_start)/(A1_num - 1) )   

A2_start = 0.0
A3_start = 0.0
A23delta = 0.05
Ndist = 0

Lambda_1 = 2*math.pi
Lambda_2 = 2*Lambda_1
Lambda_3 = 0.5*Lambda_1

b = 2*Lambda_1

i1 = b/Lambda_1
i2 = b/Lambda_2
i3 = b/Lambda_3

lam = 2*math.pi

H = 'H'
E = 'E'

sysargs = sys.argv[1:]

clpars = "p"
if sysargs != []:
  clpars = (sys.argv[1:])[0]

print clpars


exp = { 
  'B0': 0.0,  'A1': 0.0, 'i1': i1,  'A2': 0.0, 'i2': i2,  'A3': 0.0, 'i3': i3,          'B1': 0.0, 'B2': 0.0, 'B3': 0.0, 
  'D0': -3.0, 'C1': 0.0, 'j1': 0.0, 'C2': 0.0, 'j2': 0.0, 'C3': 0.0, 'j3': 0.0,         'D1': 0.0, 'D2': 0.0, 'D3': 0.0,  
  'b': 12.56637061435917, 'pol': E, 'N': 100, 'M': 100, 'alpha': 30.0, 'eps_re': 42.0, 'eps_im': 115.0, 'lam': lam,  'layer_type': 3
}


####################
# DATA PREPARATION #
####################

if "i" in clpars:

  print "Basic parameters:\n", exp, "\n"

  handle = open("input.txt", "w")

  print "Constructing experiments..."

  A2_range=[]
  A3_range=[]
  A2_range.append( A2_start )
  A3_range.append( A3_start )
  for i in range( Ndist ):
    A2_range.append( numpy.random.normal( A2_start, A23delta, None) )
    A3_range.append( numpy.random.normal( A3_start, A23delta, None) )  

  print A2_range

  for A2,A3 in zip(A2_range, A3_range):
    for A1 in A1_range:
      exp['A1'] = A1
      exp['A2'] = round( A2, 3 )
      exp['A3'] = round( A3, 3 )

      exp_str = str( exp )+'\n'
      exp_str = exp_str.replace( "'H'", "H" )
      exp_str = exp_str.replace( "'E'", "E" )
  
      print exp_str
      handle.write( exp_str )

  handle.close()

#sys.exit()


########################
# RUNNING COMPUTATIONS #
########################

if "c" in clpars:
  print "Starting computations.."
  os.system("./rs")
 
 
###########
# WAITING #
###########

if "w" in clpars:
  print "Waiting for computations to complete..."
  
  pathstr = "./output/output.txt"
  
  exists = os.path.isfile( pathstr )
  while not exists:
    time.sleep(1)
    exists = os.path.isfile( pathstr )
  


##################
# POSTPROCESSING #
##################

if "p" in clpars:

  es=[]
  R0=[]
  
  if runos == "linux":
    os.system( "cp ./output/output.txt ./postprocessing/" )
  else:
    os.system( "copy .\\output\\output.txt .\\postprocessing\\" )

  Nexps = 0
  with open('./postprocessing/output.txt', 'r') as f:
    print 'Loaded input data:'
    for y in [x.rstrip() for x in f.readlines()]:
      print y
      y = y.replace( 'nan', '-100', 4 )
      t = eval(y) 
      es.append( t ) 
      R0.append(t['R0'])
      Nexps = Nexps + 1
      print t

  print R0

  n = 1
  while n*A1_num <= Nexps:
    with open('./postprocessing/graph'+str(n)+'.txt', 'w') as f:  
      print 'Constructing curve ' + str(n)
      for i in range(A1_num):
        if R0[ i + (n-1)*A1_num ] >= 0.0 and R0[ i + (n-1)*A1_num ] <= 1.0:
          #print A1_range[ i ], deffs[ i + (n-1)*A1_num ]
          f.write( str( A1_range[ i ] ) + " " + str( R0[ i + (n-1)*A1_num ]) + '\n' )
      #if R0[ A1_num-1 + (n-1)*A1_num ] < 0.5:
        #print n
    n = n + 1
  Ngraphs = n - 1
    

  gp_base_script_name = "plotscript_R0FromA1_00.txt"
  gp_script_name = "plotscript_R0FromA1.txt"

  if runos == "linux":
    os.system( "cp ./postprocessing/scripts/"+gp_base_script_name+" ./postprocessing/"+gp_script_name )
  else:
    os.system( "copy .\\postprocessing\\scripts\\"+gp_base_script_name+" .\\postprocessing\\"+gp_script_name )
  
  plotstr = "plot "
  
  with open('./postprocessing/'+gp_script_name, 'a') as f:  
    for n in range( Ngraphs - 1 ):
      plotstr = plotstr + "'graph"+str(n+2)+".txt' notitle with lp lt 0 pt 12 lc rgb 'gray' lw 1 ps 1, "
    plotstr = plotstr + " 'graph1.txt' title 'A_2, A_3 = 0' with lp pt 20 lw 2 lc rgb 'red' ps 2"
#    plotstr = plotstr + ", 'graph1.txt' notitle with p pt 19 pc rgb 'black' ps 2"
    #print plotstr
    f.write( plotstr )
  
  if runos == "linux":
    os.system( "cd ./postprocessing; gnuplot plotscript_R0FromA1.txt" )    
  else:
    os.system( "cd .\\postprocessing; gnuplot plotscript_R0FromA1.txt" )    
  
  


