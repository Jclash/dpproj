import math, cmath
from numpy import *

eps = 42.0 + 115.0*1j

alpha = 30.0*pi/180.0

f = 2300.8 # GHZ
T = 20
ledA = 0.0026 
ledB = 0.00023

eps_led = 3.16 + (ledA/f + ledB*(pow(f,0.9)))*1j

print eps_led

t = T
Sw = 38.893
c =    3*100000000.0
fHz = f*1000000000.0
eps_0 = 5.5
eps_s = 88.2 - 0.40885*t + 0.00081*t*t
lam_s = 1.8735116 - 0.027296*t + 0.000136*t*t + 1.662*exp(-0.634*t) 
lam = 100*c/fHz
eps_1 = eps_0 + (eps_s - eps_0)/(1+(lam_s/lam)*(lam_s/lam))

eps_s = eps_s - 17.2*Sw/60.0
lam_s = lam_s - 0.206*Sw/60.0
sigma = 0.00001*(2.63*t + 77.5)*Sw
eps_2 = lam_s/lam * (eps_s - eps_0)/(1+(lam_s/lam)*(lam_s/lam))
eps_2 = eps_2 + 60.0*sigma*lam

eps_C = eps_1 - eps_2*1j

print eps_C

