
import math, cmath
from numpy import *

lam = 2*pi

eps_0 = 1.0

eps_1 = 3.0 + 3.0*1j
d1 = 0.5

eps_2 = 5.0 + 2.0*1j
d2 = 0.3

# number of periodic layers ( = actual number of layers / 2)
N = 6


Z0 = 1/sqrt(eps_0)

Z1 = 1/sqrt(eps_1)

Z2 = 1/sqrt(eps_2)

Z3 = 1/sqrt(eps_0)

Zin_1 = 0.0
Zin_2 = 0.0

for i in range(N):

  if i == 0:
    Zin_2 = Z2*(Z3 - 1j*Z2*tan(2*pi/lam*sqrt(eps_2)*d2))/(Z2 - 1j*Z3*tan(2*pi/lam*sqrt(eps_2)*d2))    
  else:
    Zin_2 = Z2*(Zin_1 - 1j*Z2*tan(2*pi/lam*sqrt(eps_2)*d2))/(Z2 - 1j*Zin_1*tan(2*pi/lam*sqrt(eps_2)*d2))
      
  Zin_1 = Z1*(Zin_2 - 1j*Z1*tan(2*pi/lam*sqrt(eps_1)*d1))/(Z1 - 1j*Zin_2*tan(2*pi/lam*sqrt(eps_1)*d1))

V = (Zin_1 - Z0)/(Zin_1 + Z0)

print "two layers. reflected:", V*conj(V)




